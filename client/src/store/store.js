import {
  createStore,
  applyMiddleware,
  compose,
  combineReducers
} from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';
import loginSaga from './sagas/profileSagas';

import profileReducer from './reducers/profileReducer';

export const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
  thunk,
  sagaMiddleware,
  routerMiddleware(history)
];

const composedEnhancers = compose(
  applyMiddleware(...middlewares),
  composeWithDevTools()
);

const rootReducer = combineReducers({
  router: connectRouter(history),
  profile: profileReducer,
});

const initialState = {
  profile: null,
};

const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers
);

sagaMiddleware.run(loginSaga);

export default store;
