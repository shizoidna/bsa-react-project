import { LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT } from '../actions/profileActionTypes';

export default (state = {}, { type, payload }) => {
  switch (type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        currentProfile: payload.profile,
      };
    case LOGIN_FAIL:
    case LOGOUT:
      return {
        ...state,
        currentProfile: null,
      };
    default:
      return state;
  }
};
