import { put, takeLatest } from 'redux-saga/effects';
import * as authService from '../../services/authService';
import { LOGIN } from '../actions/profileActionTypes';
import { loginFail, loginSuccess } from '../actions/profileActions';

function* login(action) {
  try {
    const profile = yield authService.login(action.payload.email, action.payload.password);
    yield put(loginSuccess(profile));
  } catch (e) {
    yield put(loginFail(e));
  }
}

function* loginSaga() {
  yield takeLatest(LOGIN, login);
}

export default loginSaga;
