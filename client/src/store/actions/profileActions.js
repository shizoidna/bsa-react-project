import { LOGIN, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT } from './profileActionTypes';

export const login = credentials => async dispatch => dispatch({
  type: LOGIN,
  payload: credentials
});

export const loginSuccess = profile => ({
  type: LOGIN_SUCCESS,
  payload: {
    profile
  }
});

export const loginFail = error => ({
  type: LOGIN_FAIL,
  payload: {
    error
  }
});

export const logout = () => ({
  type: LOGOUT,
});
