import React from 'react';
import { render } from 'react-dom';

import './styles/reset.scss';
import 'semantic-ui-css/semantic.min.css';
import './styles/common.scss';
import Home from './scenes/Home';

const target = document.getElementById('root');
render(
  <Home />, target
);
