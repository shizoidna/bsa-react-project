import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import Routing from 'src/components/Routing';
import store, { history } from 'src/store/store';

const Home = () => (
  <Provider store={store}>
    <ConnectedRouter history={history} />
    <Routing />
  </Provider>
);

export default Home;
