import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import LoginPage from 'src/components/LoginPage';

import NotFound from 'src/scenes/NotFound';
import PublicRoute from 'src/components/PublicRoute';

const Routing = () => (
  <div className="fill">
    <main className="fill">
      <BrowserRouter>
        <Switch>
          <PublicRoute exact path="/login" component={LoginPage} />
          <Route path="*" exact component={NotFound} />
        </Switch>
      </BrowserRouter>
    </main>
  </div>
);

Routing.propTypes = {};

Routing.defaultProps = {};

export default Routing;
