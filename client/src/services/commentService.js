import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const updateComment = async (commentId, commentText, userId) => {
  const response = await callWebApi({
    endpoint: `/api/comments/${commentId}`,
    type: 'PUT',
    request: {
      userId,
      commentId,
      commentText
    }
  });
  return response.json();
};
